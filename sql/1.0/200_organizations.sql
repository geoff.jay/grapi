CREATE TABLE app_public.organizations (
  id serial PRIMARY KEY,
  name varchar NOT NULL unique,
  created_at timestamptz NOT NULL DEFAULT now(),
  updated_at timestamptz NOT NULL DEFAULT now()
);
