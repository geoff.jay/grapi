CREATE TABLE app_public.tasks (
  id serial PRIMARY KEY,
  name text NOT NULL,
  description text,
  completed boolean,
  user_id int NOT NULL
    REFERENCES app_public.users ON DELETE CASCADE,
  created_at timestamptz NOT NULL DEFAULT now(),
  updated_at timestamptz NOT NULL DEFAULT now()
);
