insert into app_public.organizations(name) values
  ('home');

insert into app_public.users(organization_id, username, name, about, is_admin) values
  (1, 'geoff.jay@gmail.com', 'Geoff Johnson', 'This guy', true);

insert into app_public.tasks(user_id, name, description, completed) values
  (1, 'do foo', 'reminder to do that foo', true),
  (1, 'pick up a thing', 'don''t forget to pick up that thing', false),
  (1, 'try that', 'that won''t try itself', false),
  (1, 'don''t', 'don''t event think about it', true);
