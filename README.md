# Home API Project

[WIP] this isn't for anything specific yet, just testing things

## Setup

Run the database setup. Migrations are currently not handled.

```sh
psql -h db -U postgres -d home
postgres@localhost:postgres> \i sql/1.0/100_schema.sql
postgres@localhost:postgres> \i sql/1.0/200_organizations.sql
postgres@localhost:postgres> \i sql/1.0/300_users.sql
postgres@localhost:postgres> \i sql/1.0/400_tasks.sql
postgres@localhost:postgres> \i sql/1.0/999_data.sql
```

Start PostGraphile.

```sh
postgraphile -c postgres://postgres:postgres@db/home \
  --watch --enhance-graphiql --dynamic-json \
  --schema app_public \
  --host 0.0.0.0
```

Access the GraphQL API at http://localhost:5000/graphiql.
